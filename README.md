# HTML Magic Mirror

Requirements

  - Apache or nginx
  - Basic Knowlege in HTML, CSS and JS
  
### Version
1.0.0

# Configuration
Rename `/js/site.min.js.default` to `/js/site.min.js` and edit the following lines

```javascript
var Site = {
    config: {
        zip_code: 10108,
        wu_api_key: 'YOUR_API_KEY',
        owner: 'YOUR_NAME'
    },
...
```
Requires an Stratus Developer plan at https://www.wunderground.com/weather/api/d/pricing.html